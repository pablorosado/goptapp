# GOPTAPP: Graphical Optimisation of Approaches in Machine Learning projects.

## Installation.

After installing Optapp, go to goptapp folder and simply run:

	python setup.py install

## Visual example:

Here is a [YouTube video](https://www.youtube.com/watch?v=OO85T87DCMs) with an example of Goptapp in action (please do *not* share, as it is work in progress and it depends on publication of Optapp).

## Basic usage:

In a Jupyter Notebook:

	import goptapp

	goptapp.gui();
	
## Further properties:

All functionalities from Optapp are available in Goptapp (provided Optapp is available and properly installed).

	from goptapp.gui import optapp # Import module.
	pipe = optapp.pipeline.start() # Initialise pipeline.
	pipe.load_project('testing_optapp') # Load project: Load the project file (e.g. 'project_testing_optapp.py') inside project files folder.
	pipe.overview() # Overview: Displays on screen the status of all projects existing in project files folder.
	pipe.new_subset() # Create a new subset.
	pipe.load_subset() # Load a subset (by default, the last one).
	pipe.auto_runs('app_0003') # Optimise an approach: Automatically find optimal parameters of a certain approach (e.g. 'app_0003').
	pipe.compare_runs() # Compare runs and generate a results file: Display a comparison of the score of all completed runs.
	pipe.choose_optimal_run() # Choose optimal run: Choose run with best score.
	pipe.manual_runs() # Create and execute manual runs.
	pipe.choose_optimal_run(create_run_file=1) # Choose optimal run and create a TEST run.
	pipe.exec_manual_runs(fold='TEST') # Execute TEST run.
	pipe.compare_runs() # Compare again runs and generate a final results file.
	pipe.eval_approach() # Evaluate the best approach doing training/test set a number of times, to have the final evaluation.
