import ipywidgets as iw
import os, sys
import optapp

# Goptapp:
# TODO: There seems to be a bug related to par_test_frac, the slider may not be working properly.
# TODO: The following has to be executed in the notebook to set the overflow limit high enough.
#  Any way to do it automatically?
#  %%javascript
#  IPython.OutputArea.auto_scroll_threshold = 9999;
#
# Optapp:
# Short term:
# TODO: Generalise overview: Create functions to overview project_files, projects, approaches, infofiles, subsets.
# TODO: Create functions to create new project (project file, folders, infofile, and template approach).
# TODO: In new_subset, insert a bunch of try except, and make sure that, if anything goes wrong, delete created subset.
#  For that, make a function to delete subsets (asking for confirmation unless used with option force=True).
# TODO: Same when creating failed runs. Maybe create a function to clean failed runs.
# Long term:
# TODO: Command line version should work!
# TODO: There should be only one app_pars file.
#  Depending on the type of optimisation, it will be interpreted in one way or another (grid, random, or auto).
# TODO: There should be an option for random search of pars (which is more efficient than gridsearch).
#  Simply give lower and upper limits of the pars (appropriately linearised if needed).
# TODO: There should be bayesian optimisation as an option of auto method.

# IN GOPTAPP:
# TODO: Then, in goptapp, inside 'manage projects', there's an accordion called 'create project'.
#  The first level is 'create project file': here do the same having approach, infofiles and sds as input text widgets,
#  and accordion of all other pars in default_pars. One has to tick on a box to enable them and then decide the value.
#  The second level is 'create infofile', which creates a simple csv in par_INFOFILES.
#  The third level is 'create approach', which loads a template approach.
# TODO: In every function accordion there should be a 'load default pars' (which loads default pars for that project).
#  The reason is that, if you are in a particular subset and then change to another, where some undefined parameters,
#  the ones from the previous subset will be used, which may not make sense for the new subset.
# TODO: Include help also in the first 'OPTAPP' box.
# TODO: The 'Help' part could have a general overview of optapp, enumerate steps to follow and link to full tutorial.

class start(object):
    
    def __init__(self, project_files_dir = None):
        # If path to project files is not specified, get current directory.
        if not project_files_dir: project_files_dir = os.path.abspath(os.path.curdir)
        self.project_files_dir = project_files_dir # Save in class.
        # Define internal variables for titles and styles (that will change as gui is used).
        self.pipeline_on_text = 'Pipeline on'
        self.pipeline_off_text = 'Pipeline off'
        self.pipeline_on_style = 'success'
        self.pipeline_off_style = 'danger'
        self.update_only_lists = False # Switch that avoids all parameters to be updated when we just need widget lists to be updated.
        # Get all layouts in a dictionary.
        self.layouts, self.styles = self.get_layout()
        # Get list of functions and parameters.
        self.options_list = self.get_options_list()
        # Make a switch to know if pipeline is on, and initialise it switched off.
        self.pipeline_on = False 
        # Initialise dictionary that will gather all execution buttons.
        self.exec_buttons = {}
        # Initialise dictionary that will contain labels with help texts.
        self.help_labels = {}
        # Make dictionary with functions and parameter help texts.
        self.help_buttons = self.get_help_buttons(self.options_list)
        # Create main pipeline dashboard.
        self.dashboard = self.create_dashboard()
        display(self.dashboard)

    def get_options_list(self):
        '''
        Returns a dictionary with the lists of parameters for each function,
        e.g. for function 'new_subset' it returns the list ['par_brief_description', 'par_selection', ...].
        '''
        options_list = {
            'prj_overview':[],
            'overview':[],
            'new_subset':['par_brief_description', 'par_selection',
                          'par_n_examples', 'par_examples_col','par_labels_col',
                          'par_separator', 'par_random_seed', 'par_n_splits',
                          'par_cv_frac', 'par_test_frac', 'par_check'],
            'manual_runs':['approach'],
            'auto_runs':['approach','par_iterations',
                         'par_name_score'],
            'compare_runs':['par_top_runs', 'par_output_results',
                            'par_name_score', 'par_details'],
            'choose_optimal_run':['create_run_file',
                                  'par_plot_results', 'par_conditions'],
            'eval_approach':[]
        }
        return options_list

    def get_help_buttons(self,options_list):
        help_buttons = {}
        for fun in list(options_list):
            if fun in dir(optapp):
                docstring = eval('optapp.%s.%s.__doc__' %(fun,fun))
                help_text = docstring.split('Parameters')[0] # Get rid of parameter help.
                list_pars = options_list[fun]
                for line in docstring.split('-->'):
                    subline = line.split(' : ')
                    if len(subline)==2 and subline[0] in list_pars:
                        help_buttons[subline[0]] = ('Parameter: '+subline[0]+'\nType: '+subline[1]).replace('\n','<br>')
            else:
                help_text = 'Coming soon.'
            # Store help text as HTML (and get rid of author name, which is repeated in every function).
            help_buttons[fun] = help_text.replace('\n','<br>')
        return help_buttons
    
    def get_layout(self):
        '''
        Returns a dictionary with layouts of main ipywidget objects.
        '''
        start_pipeline_box = iw.Layout(
            #display = 'flex',
            align_items='stretch',
            width='100%'
        )
        options_box = iw.Layout(
            display = 'flex',
            align_items='center',
            width='100%',
            min_height='420px',
            max_height='420px'
        )
        options_box_left = iw.Layout(
            display = 'flex',
            align_items='center',
            width='50%',
            min_height='420px',
            max_height='420px'
        )
        options_box_right = iw.Layout(
            display = 'flex',
            #align_items='center',
            flex_flow='flex-start',
            border = 'solid 1px',
            width='50%',
            min_height='420px',
            max_height='420px'
        )
        wids_column = iw.Layout(
            display = 'flex',
            align_items='stretch',
            width='80%',
            min_height='420px',
            max_height='420px'
        )
        helps_column = iw.Layout(
            display = 'flex',
            align_items='center',
            width='20%',
            min_height='420px',
            max_height='420px'
        )
        question_button = iw.Layout(
            align_items='center',
            width='50%'
        )    
        output_box = iw.Layout(
            display='flex',
            flex_flow='wrap', #With 'wrap' and align_items='flex-end' it successfully wraps, but no scroll unless overflowing.
            #flex_flow='scroll', #With 'scroll' it always shows full (scrollable) output but doesn't wrap. Any way to achieve wrapping and scrolling?? 
            border='solid 1px',
            align_items='flex-end',
            overflow='unset',#['visible', 'hidden', 'scroll', 'auto', 'inherit', 'initial', 'unset']
            width='95%',
            height='90%',
            min_height='420px',
            max_height='420px'
        )
        left_column = iw.Layout(
            align_items='center',
            width='100%',
            height='90%'
        )
        right_column = iw.Layout(
            align_items='center',
            width='100%',
            height='90%'
        )
        outer_box = iw.Layout(
            display = 'flex',
            border = 'solid 1px',
            align_items='stretch',
            width='100%',
            height='500px'
        )
        text = iw.Layout(
            width='100%',
        )
        status_button = iw.Layout(
            width = '99.1%',
            align_items='stretch'
        )
        single_row = iw.Layout(
            height = '50px',
            width = '99.5%',
            align_items='stretch'
        )
        # Define styles.
        long_description_style = {'description_width': 'initial'}
        
        # Collect all layouts and styles.
        layouts = {'start_pipeline_box': start_pipeline_box,
                   'options_box': options_box,
                   'options_box_left': options_box_left,
                   'options_box_right': options_box_right,
                   'wids_column': wids_column,
                   '?_button': question_button,
                   'helps_column': helps_column,
                   'output_box': output_box,
                   'left_column': left_column,
                   'right_column': right_column,
                   'outer_box': outer_box,
                   'text': text,
                   'status_button': status_button,
                   'single_row': single_row
                  }
        styles = {'desc':long_description_style}
        return layouts, styles
    
    def start_pipeline(self):
        '''
        Returns a box with the main button to turn pipeline on.
        '''
        insert_path=iw.Text(
            value=self.project_files_dir,
            placeholder='Type path',
            description='Insert path to project files',
            layout = self.layouts['text'],
            style = self.styles['desc']
        )
        
        # Define status button style and behaviour (does not do anything on click, but changes color).
        if self.pipeline_on:
            status_button = iw.Button(description=self.pipeline_on_text, button_style = self.pipeline_on_style, layout = self.layouts['status_button'])
        else:
            status_button = iw.Button(description=self.pipeline_off_text, button_style = self.pipeline_off_style, layout = self.layouts['status_button'])     
        def on_change(change):
            if change['type'] == 'change' and change['name'] == 'description':
                #self.dashboard.close() # Instead of closing it here, close it in the last moment, before replacing with new one.
                self.dashboard = self.create_dashboard()
                display(self.dashboard)
        status_button.observe(on_change)

        # Create switch button to start/stop pipeline.
        def switch_function(_):
            path = os.path.abspath(insert_path.get_interact_value())
            if os.path.isdir(path):
                self.project_files_dir = path
                self.project_list = [file.split('project_')[-1].split('.py')[0] for file in os.listdir(path) if (os.path.isfile(os.path.join(path,file)) and file.startswith('project_') and file.endswith('.py'))] 
                if len(self.project_list)>0:
                    self.oa = optapp.pipeline.start(project_files_dir=path)
                    self.pipeline_on = True
                    status_button.description = self.pipeline_on_text
                else:
                    self.pipeline_on = False
                    self.pipeline_off_text = 'Pipeline off (no projects found!)'
                    self.pipeline_off_style = 'warning'
                    status_button.description = self.pipeline_off_text
            else:
                self.pipeline_on = False
                self.pipeline_off_text = 'Pipeline off (invalida path!)'
                self.pipeline_off_style = 'danger'
                status_button.description = self.pipeline_off_text
        switch_button = iw.Button(description = 'Start pipeline', button_style='info')
        switch_button.on_click(switch_function)
        second_row = iw.Box(children=[insert_path, switch_button], layout = self.layouts['single_row'])
        return iw.VBox([status_button, second_row], layout = self.layouts['start_pipeline_box'])

    def get_approach_list(self,approaches_dir):
        '''
        Returns a tuple with all approaches existing in folder 'approaches_dir'.
        '''
        if os.path.isdir(approaches_dir):
            app_list = tuple(sorted([file.split('_main.py')[0] for file in os.listdir(approaches_dir) if file.startswith('app_')
                             and file.endswith('_main.py')]))
        else:
            print("Warning: Invalid directory of approaches %s" %approaches_dir)
            app_list = []
        return app_list
    
    def get_par_sds_num_list(self, par_SUBDATASETS):
        '''
        Returns a tuple with all subsets existing in folder 'par_SUBDATASETS'.
        '''
        if os.path.isdir(par_SUBDATASETS):
            sds_list = tuple(sorted([file for file in os.listdir(par_SUBDATASETS) if os.path.isdir(par_SUBDATASETS+file)]))
        else:
            print("Warning: Invalid directory of subsets %s" %par_SUBDATASETS)
            sds_list = []
        return sds_list
    
    def get_widgets(self):
        '''
        Returns a dictionary with all parameter widgets, e.g. a dropdown to choose subset number (parameter 'par_sds_num').
        Each widget is given as a tuple of two elements:
        + The 0th element is the type of parameter, i.e. float, int, or str.
        + The 1st element is the widget (as an ipywidgets object).
        '''
        # Initialise dictionary containing, for each parameter, a tuple (type, widget).
        wids = {}
        wid_style = self.styles['desc'] # Widget style to ensure full description can be read.
        
        # Main parameters: project, approach, and subset widgets.
        wids['project'] = (str, iw.Dropdown(options = self.project_list, description='Project'))
        wids['project'][1].value = self.project_list[0] # Arbitrary choice: take first.
        self.oa.load_project(wids['project'][1].value)
        approach_list = self.get_approach_list(self.oa.par.par_APPROACHES)
        wids['approach']= (str, iw.Dropdown(options = approach_list, description='Approach', style=wid_style))
        par_sds_num_list = self.get_par_sds_num_list(self.oa.par.par_SUBDATASETS)
        wids['par_sds_num'] = (int, iw.Dropdown(options = par_sds_num_list, description='Subset number', style=wid_style))
        if len(par_sds_num_list)>0:
            self.oa.load_subset(int(wids['par_sds_num'][1].value))
                
        # Parameters for new_subset:
        wids['par_n_examples'] = (int, iw.IntText(step=5, description='Number of examples', style=wid_style)) # Better: count number of rows in infofile and set boundaries. Set a timeout to avoid it to take too long if infofile is big.
        wids['par_examples_col'] = (str, iw.Text(description='Column name of examples', style=wid_style))
        wids['par_labels_col'] = (str, iw.Text(description='Column name of labels', style=wid_style))
        wids['par_separator'] = (str, iw.Text(description='Separator', style=wid_style))
        wids['par_selection'] = (str, iw.Text(description='Selection', style=wid_style))
        wids['par_random_seed'] = (int, iw.IntText(step=10, description='Random seed', style=wid_style))
        wids['par_n_splits'] = (int, iw.IntText(step=1, description='Number of folds', style=wid_style))
        wids['par_cv_frac'] = (float, iw.FloatSlider(min=0, max=1.0, step=0.1, description='Fraction of CV set', continuous_update=False, orientation='horizontal', readout=True, readout_format='.1f', style=wid_style))
        def one_split(change):
            '''
            CV fraction should only be visible if number of splits is 1.
            '''
            if change['type'] == 'change' and change['name'] == 'value':
                wids['par_cv_frac'][1].layout.visibility = 'visible' if wids['par_n_splits'][1].value==1 else 'hidden'
        wids['par_n_splits'][1].observe(one_split)
        wids['par_test_frac'] = (float, iw.FloatSlider(min=0, max=1.0, step=0.1, description='Fraction of test set', continuous_update=False, orientation='horizontal', readout=True, readout_format='.1f', style=wid_style))
        wids['par_check'] = (int, iw.RadioButtons( options=[0, 1],description = 'Consistency check? (0 no/1 yes)', style=wid_style))
        wids['par_brief_description'] = (str, iw.Text(description='Brief description', style=wid_style))

        # Parameters for auto_runs:
        wids['par_iterations'] = (int, iw.BoundedIntText(min=1, max=1000, step=5, description='Number of iterations', style=wid_style))    
        wids['par_name_score'] = (str, iw.Text(description='Score name', style=wid_style))

        # Parameters for compare_runs:
        wids['par_output_results'] = (int, iw.RadioButtons( options=[0, 1],description = 'Output results? (0 no/1 yes)', style=wid_style))
        wids['par_details'] = (int, iw.RadioButtons( options=[0, 1],description = 'Show details? (0 no/1 yes)', style=wid_style))
        wids['par_top_runs'] = (int, iw.IntText(step=10, description='Number of runs to display', style=wid_style))
        #wids['par_conditions'] = (list, iw.Text(description='Conditions (for display)', style=wid_style))        
        
        def update_pars_on_click(change):
            if change['type'] == 'change' and change['name'] == 'value' and self.update_only_lists==False:
                self.update_pars(wids)
            
        # Update parameters and let project and subset widgets update when something changes.
        self.update_pars(wids)
        wids['project'][1].observe(update_pars_on_click)
        wids['par_sds_num'][1].observe(update_pars_on_click)
        
        return wids
    
    def update_pars(self, wids):
        '''
        Updates list of approaches and subsets.
        
        '''
        # Load project.
        self.oa.load_project(wids['project'][1].value)
        # Update list of approaches.
        approach_list=self.get_approach_list(self.oa.par.par_APPROACHES)
        wids['approach'][1].options = approach_list
        # Update list of subsets.
        par_sds_num_list = self.get_par_sds_num_list(self.oa.par.par_SUBDATASETS)
        wids['par_sds_num'][1].options = par_sds_num_list
        if len(par_sds_num_list)>0 and wids['par_sds_num'][1].value!=None:
            self.oa.load_subset(int(wids['par_sds_num'][1].value))
            wids['par_sds_num'][1].value = '%.4i' %self.oa.par.par_sds_num
        #Update all other parameters.
        for wid_i in list(wids):
            if wid_i.startswith('par_') and not wid_i=='par_sds_num':
                wids[wid_i][1].value = eval('self.oa.par.%s' %wid_i)
    
    def update_lists(self, wids):
        '''
        Updates widgets lists without doing a full update of all parameters.
        This is useful after using a function.
        For example, after using new_subset, the widget list of subsets should be updated, without changing the current value.
        '''
        # Update list of subsets.
        par_sds_num_list = self.get_par_sds_num_list(self.oa.par.par_SUBDATASETS)
        # When changing a widget list, the value will automatically change to the first in the list.
        current_par_sds_num = wids['par_sds_num'][1].value
        wids['par_sds_num'][1].options = par_sds_num_list
        wids['par_sds_num'][1].value = current_par_sds_num
        
    def enable_exec_buttons(self, exec_buttons):
        '''
        Enables buttons when no function is running.
        '''
        for button_i in list(exec_buttons):
            if button_i.endswith(('_full_output','_clear_output')):
                exec_buttons[button_i].disabled=False
            else:
                exec_buttons[button_i].description='Go!'
                exec_buttons[button_i].button_style='success'
                exec_buttons[button_i].disabled=False

    def disable_exec_buttons(self, exec_buttons):
        '''
        Disables buttons when a function is running.
        '''
        for button_i in list(exec_buttons):
            if button_i.endswith(('_full_output','_clear_output')):
                exec_buttons[button_i].disabled=True
            else:
                exec_buttons[button_i].description='Busy'
                exec_buttons[button_i].button_style='warning'
                exec_buttons[button_i].disabled=True
        
    def enable_wids(self, wids):
        '''
        Enables widgets when no function is running.
        '''
        for wid_i in list(wids):
            wids[wid_i][1].disabled=False

    def disable_wids(self, wids):
        '''
        Disables widgets when a function is running.
        '''
        for wid_i in list(wids):
            wids[wid_i][1].disabled=True
            
    def create_function_box(self, function, box_type = 1):
        '''
        Returns a box with an option column and an outputs column for a certain function of the pipeline. 
        '''
        # Get pre-defined layouts.
        layouts = self.layouts
        
        #Load all widgets and select those that are arguments of this function.
        args_for_function = self.options_list[function.__name__]
        common_keys = sorted(set(list(self.wids)) & set(args_for_function) , key = args_for_function.index)
        wids_sel = {key: self.wids[key][1] for key in common_keys}
        types_sel = {key: self.wids[key][0] for key in common_keys}
        wids_list = [wids_sel[key] for key in list(wids_sel)]
        # Create a list of widget help buttons.
        wids_helps = []
        self.help_labels[function.__name__] = iw.HTML()
        for key in list(wids_sel)+[function.__name__]: # Create help button for each selected widget and for the function itself.
            wids_helps.append(iw.Button(description = "?", layout = layouts['?_button']))
            # Add identifier to this button.
            wids_helps[-1].help_button_id = key
            def help_click(_):
                key = _.help_button_id #Use this key to access a dictionary of help texts.
                if key in list(self.help_buttons):
                    help_text = self.help_buttons[key]
                else:
                    help_text = key+': Coming soon.' 
                self.help_labels[function.__name__].value=help_text
            wids_helps[-1].on_click(help_click)
        # Start help window with the help text of the corresponding function.
        self.help_labels[function.__name__].value = self.help_buttons[function.__name__]
                             
        # Create execution button.        
        def execute_function(_):
            wids_vals={}
            # Get execution button in dashboard.
            for key in list(wids_sel):
                w_val = wids_sel[key].get_interact_value()
                w_type = types_sel[key]
                wids_vals[key] = w_type(w_val)
            with output_box:
                try:
                    self.disable_exec_buttons(self.exec_buttons)
                    self.disable_wids(self.wids)
                    function(**wids_vals)
                    # After applying a function, update pars. For example, after creating a new subset, this will update list of subsets.
                    self.update_only_lists=True # This is a workaround to avoid values to automatically change to first element when updating a widget list.
                    self.update_lists(self.wids)
                    self.update_only_lists=False
                    self.enable_exec_buttons(self.exec_buttons)
                    self.enable_wids(self.wids)
                except SystemExit:
                    print('\nDone.')
                except KeyboardInterrupt:
                    print('ERROR: Execution cancelled by user!')
                except Exception as e:
                    print(e)
                    print('ERROR: Something went wrong!')
                self.enable_exec_buttons(self.exec_buttons)
                self.enable_wids(self.wids)
        self.exec_buttons[function.__name__] = iw.Button()
        self.exec_buttons[function.__name__].on_click(execute_function)
        self.enable_exec_buttons(self.exec_buttons)
        self.enable_wids(self.wids)
        
        # Create boxes.
        output_box = iw.Output(layout=layouts['output_box'])
        if box_type == 3:
            options_box = iw.VBox(children=wids_list+[self.exec_buttons[function.__name__]], layout = layouts['options_box'])
            left_column = iw.VBox(children=[iw.Label(value='Parameters'), options_box], layout = layouts['left_column'])
            right_column = iw.VBox(children=[iw.Label(value='Outputs'), output_box], layout = layouts['right_column'])
            outer_box = iw.HBox(children=[left_column, right_column], layout = layouts['outer_box'])
            
        elif box_type == 2:
            options_box = iw.VBox(children=wids_list+[self.exec_buttons[function.__name__]], layout = layouts['options_box'])
            # Add button to show full/wrap output.
            self.exec_buttons[function.__name__+'_full_output'] = iw.ToggleButton(value=False, description='Show full output', tooltip = 'Press to show full output/wrap output')
            def full_output_button_on_change(change):
                if change['type'] == 'change' and change['name'] == 'value':
                    if output_box.layout.flex_flow == 'wrap':
                        output_box.layout.flex_flow = 'scroll'
                        output_box.layout.align_items = 'initial'
                        self.exec_buttons[function.__name__+'_full_output'].description = 'Wrap output'
                    else:
                        output_box.layout.flex_flow = 'wrap'
                        output_box.layout.align_items = 'flex-end'
                        self.exec_buttons[function.__name__+'_full_output'].description = 'Show full output'
            self.exec_buttons[function.__name__+'_full_output'].observe(full_output_button_on_change)
            # Add button to clear output.
            self.exec_buttons[function.__name__+'_clear_output'] = iw.Button(description="Clear outputs", button_style = 'danger')
            def clear_output_button_on_click(_):
                output_box.clear_output()
            self.exec_buttons[function.__name__+'_clear_output'].on_click(clear_output_button_on_click)
            # Prepare output boxes.
            output_buttons = iw.HBox(children=[self.exec_buttons[function.__name__+'_full_output'], self.exec_buttons[function.__name__+'_clear_output']])
            output_column = iw.VBox(children=[output_buttons, output_box])
            tabs = iw.Tab(children = [options_box, output_column])
            #tabs = iw.Tab(children = [options_box, output_box])
            #tabs.children = [options_box, output_box]
            tabs.set_title(0, 'Parameters')
            tabs.set_title(1, 'Outputs')
            #outer_box = iw.HBox(children=tabs, layout = layouts['outer_box'])
            outer_box = tabs
            
        elif box_type == 1:
            wids_box = iw.VBox(children = wids_list+[self.exec_buttons[function.__name__]], layout = layouts['wids_column'])
            helps_box = iw.VBox(children = wids_helps, layout = layouts['helps_column'])
            options_box_left = iw.HBox(children=[wids_box, helps_box], layout = layouts['options_box_left'])
            options_box_right = iw.Box(children = [self.help_labels[function.__name__]], layout = layouts['options_box_right'])
            options_box = iw.HBox(children = [options_box_left, options_box_right])
            # Add button to show full/wrap output.
            self.exec_buttons[function.__name__+'_full_output'] = iw.ToggleButton(value=False, description='Show full output', tooltip = 'Press to show full output/wrap output')
            def full_output_button_on_change(change):
                if change['type'] == 'change' and change['name'] == 'value':
                    if output_box.layout.flex_flow == 'wrap':
                        output_box.layout.flex_flow = 'scroll'
                        output_box.layout.align_items = 'initial'
                        self.exec_buttons[function.__name__+'_full_output'].description = 'Wrap output'
                    else:
                        output_box.layout.flex_flow = 'wrap'
                        output_box.layout.align_items = 'flex-end'
                        self.exec_buttons[function.__name__+'_full_output'].description = 'Show full output'
            self.exec_buttons[function.__name__+'_full_output'].observe(full_output_button_on_change)
            # Add button to clear output.
            self.exec_buttons[function.__name__+'_clear_output'] = iw.Button(description="Clear outputs", button_style = 'danger')
            def clear_output_button_on_click(_):
                output_box.clear_output()
            self.exec_buttons[function.__name__+'_clear_output'].on_click(clear_output_button_on_click)
            # Prepare output boxes.
            output_buttons = iw.HBox(children=[self.exec_buttons[function.__name__+'_full_output'], self.exec_buttons[function.__name__+'_clear_output']])
            output_column = iw.VBox(children=[output_buttons, output_box])
            tabs = iw.Tab(children = [options_box, output_column])
            #tabs = iw.Tab(children = [options_box, output_box])
            #tabs.children = [options_box, output_box]
            tabs.set_title(0, 'Parameters')
            tabs.set_title(1, 'Outputs')
            #outer_box = iw.HBox(children=tabs, layout = layouts['outer_box'])
            outer_box = tabs
        return outer_box

    def create_dashboard(self):
        '''
        Returns the main dashboard: an accordion with all functionalities. 
        '''
        # Create accordion.
        acc = iw.Accordion()
        empty = iw.Label(value='Coming soon')
        inactive = iw.Label(value='Pipeline not active!')

        # First box contains a switch to turn pipeline on (by giving appropriate project files folder).
        start_pipeline_box = self.start_pipeline()
        
        # Get all widgets if pipeline is on (otherwise it would fail).
        if self.pipeline_on:
            self.wids = self.get_widgets()
        
        # Define boxes with the appropriate widgets of functions.
        # Manage projects boxes.
        self.select_project_box = iw.VBox(children=[self.wids['project'][1]]) if self.pipeline_on else inactive
        self.review_project_box = iw.VBox(children=[empty]) if self.pipeline_on else inactive
        self.new_project_box = iw.VBox(children=[empty]) if self.pipeline_on else inactive
        # Manage subsets boxes.
        self.select_subset_box = iw.VBox(children=[self.wids['par_sds_num'][1]]) if self.pipeline_on else inactive
        self.review_subset_box = iw.VBox(children=[self.create_function_box(self.oa.overview)]) if self.pipeline_on else inactive
        self.new_subset_box = self.create_function_box(self.oa.new_subset) if self.pipeline_on else inactive
        # Manage runs boxes.
        self.manual_runs_box = self.create_function_box(self.oa.manual_runs) if self.pipeline_on else inactive
        self.auto_runs_box = self.create_function_box(self.oa.auto_runs) if self.pipeline_on else inactive
        self.compare_runs_box = self.create_function_box(self.oa.compare_runs) if self.pipeline_on else inactive
        # Manage results boxes.
        self.choose_optimal_run_box = self.create_function_box(self.oa.choose_optimal_run) if self.pipeline_on else inactive
        self.eval_approach_box = self.create_function_box(self.oa.eval_approach) if self.pipeline_on else inactive
        
        # Sub-accordion for projects.
        acc_projects = iw.Accordion(children = [self.select_project_box, self.review_project_box, empty])
        acc_projects.set_title(0, 'Select project')
        acc_projects.set_title(1, 'Review existing projects')
        acc_projects.set_title(2, 'Create new project')
        acc_projects.selected_index = None
        
        # Sub-accordion for subsets.
        acc_subsets = iw.Accordion(children = [self.select_subset_box, self.review_subset_box, self.new_subset_box])
        acc_subsets.set_title(0, 'Select subset')
        acc_subsets.set_title(1, 'Review existing subsets')
        acc_subsets.set_title(2, 'Create new subset')
        acc_subsets.selected_index = None # Initialise closed.
        
        # Sub-accordion for runs.
        acc_runs = iw.Accordion(children = [self.compare_runs_box, self.auto_runs_box, self.manual_runs_box])
        acc_runs.set_title(0, 'Compare runs')
        acc_runs.set_title(1, 'Automatic optimisation')
        acc_runs.set_title(2, 'Manual optimisation')
        acc_runs.selected_index = None # Initialise closed.
        
        # Sub-accordion for results.
        acc_results = iw.Accordion(children = [self.choose_optimal_run_box, self.eval_approach_box])
        acc_results.set_title(0, 'Choose best approach')
        acc_results.set_title(1, 'Final evaluation')
        acc_results.selected_index = None # Initialise closed.
        
        # Collect all sub-accordions into main accordion.
        acc.children = [start_pipeline_box,
                        acc_projects,
                        acc_subsets,
                        acc_runs,
                        acc_results,
                        empty]
        acc.set_title(0, 'OPTAPP')
        acc.set_title(1, 'Manage projects') # This should run a sort of overview for projects (add 'description=' to each one of them) and create_new_project (which helps create a project file, an infofile, and an approach). 
        acc.set_title(2, 'Manage subsets') # This should run overview, and new_subset.
        acc.set_title(3, 'Manage runs') # This should run manual_runs and auto_runs.
        acc.set_title(4, 'Manage results') # This should run compare_results, choose_best.
        acc.set_title(5, 'Help')
        
        # If dashboard already exists, close it before replacing it with a new one.
        if 'dashboard' in dir(self):
            self.dashboard.close()
        return acc
