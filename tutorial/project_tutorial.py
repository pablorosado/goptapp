# Project tutorial: dummy project to show how optapp/goptapp work.

# Define required paths.
par_INFOFILES='/Users/prosado/Documents/projects/tutorial/infofiles/'
par_APPROACHES='/Users/prosado/Documents/projects/tutorial/code/approaches/'
par_SUBDATASETS='/Users/prosado/Documents/projects/tutorial/subdatasets/'

# Define other options.
par_n_splits = 2
par_labels_col = "label"
par_parlist_plot = ["approach", "app_0000_example_float"]