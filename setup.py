from setuptools import setup

#Load long description from README file.
long_description = open("README.md").read()

############################################

setup(name="goptapp",
    packages=["goptapp"],
    version="0.1",
    description="Graphical OptApp.",
    author="Pablo A. Rosado",
    author_email="mail@pablorosado.com",
    long_description=long_description,
    #url="https://github.com/...",
    #download_url ="https://github.com/...",
    keywords=["Machine Learning"],
    classifiers=[
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "License :: Other/Proprietary License",
        "Operating System :: OS Independent",
        "Intended Audience :: Developers",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering",
        "Topic :: Scientific/Engineering :: Artificial Intelligence",
        "Topic :: Software/Development",
    ],
    license="Proprietary",
    install_requires=[
        "numpy",
        "ipywidgets",
        #"optapp",
    ],
    #scripts=["..."],
    #package_data = {"...": ["data/*.csv"]},
    zip_safe=False)
